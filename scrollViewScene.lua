local composer = require( "composer" )
local widget = require( "widget" )

local json = require("json");

local http = require("socket.http")
local ltn12 = require("ltn12")


local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------

-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    --текущая картинка на экране
    local currentBackgroundPictures = 1
    --список файлов
    local downloadImages = {}

	local viewableScreenW, viewableScreenH = display.viewableContentWidth, display.viewableContentHeight

	local myPict

    --функция загрузки из json
	function loadSettings(filename)
		-- получаем путь к файлу
		local path = system.pathForFile(filename, system.ResourceDirectory);
		local contents = "";
		local myTable = {};
		local file = io.open(path, "r"); -- открываем файл
		if (file) then -- если такой файл существует
			 local contents = file:read( "*a" ); -- читаем из него данные
			 myTable = json.decode(contents); -- расшифровываем их
			 io.close(file); -- закрываем файл
			 return myTable; -- возвращаем параметры из файла
		end
		return nil
	end

	local function turnActivityIndicator( set )
		-- Activity Indicator
		native.setActivityIndicator( set )
	end

	--загрузка файлов из сети
	local function loadImage( nameFile, linkFile)
		-- Create local file for saving data
		local path = system.pathForFile( nameFile, system.TemporaryDirectory ) .. ".jpg"
		local myFile = io.open( path, "w+b" ) 

		-- Запрос удаленного файла
		http.request{ 
	    	url = linkFile,
	    	sink = ltn12.sink.file(myFile),
		}
		-- Activity Indicator
		turnActivityIndicator(false)

	end

	--вписать изображение в экран
	local function resizePictures( tmpPictures )
		
		if tmpPictures.width > viewableScreenW or tmpPictures.height > viewableScreenH then
			if tmpPictures.width/viewableScreenW > tmpPictures.height/viewableScreenH then 
					tmpPictures.xScale = viewableScreenW/tmpPictures.width
					tmpPictures.yScale = viewableScreenW/tmpPictures.width
			else
					tmpPictures.xScale = viewableScreenH/tmpPictures.height
					tmpPictures.yScale = viewableScreenH/tmpPictures.height
			end		
		end

		tmpPictures.x = display.contentWidth *.5
		tmpPictures.y = viewableScreenH *.5
		return tmpPictures
	end

	--следующий файл 
	local function nextPictures( i )

		myPict:removeSelf()

		myPict = display.newImage( downloadImages[i],system.TemporaryDirectory, display.contentCenterX, display.contentCenterY)

		tmp = resizePictures(myPict)

		myPict:scale(tmp.xScale , tmp.yScale)

	end

	-- загрузим файл со ссылками
	local tmp_table = loadSettings("images.json");

	turnActivityIndicator(true)

	for key, value in pairs(tmp_table.images) do
		load_temp_image = loadImage(key, value)
		--print(key.. ".jpg")
		table.insert(downloadImages, key, key .. ".jpg")
	end		

	timer.performWithDelay( 400, turnActivityIndicator(false))


	-- scrollView listener
	local function scrollListener( event )
		local phase = event.phase
		local direction = event.direction

	    -- In the event a scroll limit is reached...
	    if ( event.limitReached ) then
	        if ( event.direction == "up" ) then
	        	if (currentBackgroundPictures < #downloadImages) then
	        		currentBackgroundPictures = currentBackgroundPictures + 1
	        		nextPictures(currentBackgroundPictures)
	        	end
	        elseif ( event.direction == "down" ) then
	        	if (currentBackgroundPictures >= 2) then
	        		currentBackgroundPictures = currentBackgroundPictures - 1
	        		nextPictures(currentBackgroundPictures)
	        	end
	        end
	    end
			return true
		end

		-- Create a scrollView
		scrollView = widget.newScrollView {
			left = 0,
			top = 15,
			width = display.contentWidth,
			height = display.contentHeight-30,
			hideBackground = false,
			--backgroundColor = {0.8, 0.8, 0.8},
			backgroundColor = {0, 0, 0},
			isBounceEnabled = false,
			horizontalScrollDisabled = true,
			verticalScrollDisabled = false,
			listener = scrollListener
		}
		sceneGroup:insert( scrollView )

		--при пролистывании изменим текущий файл
		myPict = display.newImage( downloadImages[currentBackgroundPictures],system.TemporaryDirectory, display.contentCenterX, display.contentCenterY)

		--впишем в размер экрана
		tmp = resizePictures(myPict)
		myPict:scale(tmp.xScale , myPict.yScale)

		scrollView:insert( myPict )

	end
-- "scene:show()"
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).
    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.
    end
end

-- "scene:hide()"
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
    end
end
 
 
-- "scene:destroy()"
function scene:destroy( event )
 
    local sceneGroup = self.view
 
    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end

	scene:addEventListener( "create" )
	scene:addEventListener( "show", scene )
	scene:addEventListener( "hide", scene )
	scene:addEventListener( "destroy", scene )

return scene